'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.hasOne(models.Biodata, {
        foreignKey: 'userId',
        unique: true
      })
      User.hasMany(models.History, {
        foreignKey: 'userId'
      });
    }
  }
  User.init({
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        len: 5,
        isAlphanumeric: true
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        is: /^[0-9a-f]{8}$/i
      }
    }
  }, {
    sequelize,
    modelName: 'User',
  });

  User.addHook('afterValidate', async (user, options) => {
    const salt = await bcrypt.genSalt();
    user.password = await bcrypt.hash(user.password, salt);
  });

  User.prototype.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
  }

  return User;
};