const express = require("express");
const bodyParser = require("body-parser");
const session = require("express-session");
const cookieParser = require("cookie-parser");

const { User, Biodata, History } = require("./models");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

const app = express();

const oneDay = 1000 * 60 * 60 * 24;

app.use(session({
    secret: "secret",
    saveUninitialized: true,
    cookie: { maxAge: oneDay },
    resave: false
}));

app.use(function (req, res, next) {
    res.locals.userId = req.session.userId;
    next();
});

app.use(cookieParser());

app.set('view engine', 'ejs');

const port = "3000";

const moment = require('moment');
const date = new Date()
const utcDate = moment(date).tz('Asia/Jakarta').format('DD-MM-YYY HH:mm:ss');

// Views
app.get("/", async (req, res) => {
    const user = await User.findByPk(req.session.userId);

    if (user) {
        const biodata = await Biodata.findOne({
            where: {
                userId: user.id
            }
        });

        const history = await History.findAll({
            where: {
                userId: user.id
            }
        });

        user.history = history;

        res.render('home', {
            session: req.session,
            user: user,
            biodata: biodata,
            history: history,
        });
    } else {
        res.render('home', {
            session: req.session,
            user: null,
            biodata: null,
            history: null
        });
    }
});
app.get("/biodata", (req, res) => {
    if (!req.session.userId) {
        return res.redirect('/')
    }

    res.render('biodata/create', {
        session: req.session
    })
})

app.get("/biodata/edit", async (req, res) => {
    try {
        const user = await User.findByPk(req.session.userId);
        const biodata = await Biodata.findOne({ where: { userId: user.id } });
        res.render("biodata/edit", { biodata });
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});


app.get("/history", (req, res) => {
    if (!req.session.userId) {
        return res.redirect('/')
    }
    res.locals.moment = moment;
    res.render('history/create', {
        session: req.session
    })
})


app.get("/history/edit/:id", async (req, res) => {
    History.findByPk(req.params.id)
        .then(history => {
            res.render('history/edit', {
                history: history
            })
        })
})

app.get("/login", (req, res) => {
    if (req.session.userId) {
        return res.redirect('/');
    }

    res.render('users/login', {
        session: req.session
    });
});

app.get("/register", (req, res) => {
    if (req.session.userId) {
        return res.redirect('/');
    }

    res.render('users/register', {
        session: req.session
    });
});

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// API Authentication
app.post("/api/login", urlEncoded, async (req, res) => {
    let message = {
        type: 'error',
        message: 'Login failed, username/password does not match'
    };

    try {
        let user = await User.findOne({
            where: {
                username: req.body.username
            }
        });

        if (!user.validPassword(req.body.password)) {
            return res.render('users/login', {
                message: message
            })
        }

        // Store to session
        req.session.userId = user.id;
        res.redirect('/login');
    } catch (error) {
        res.render('users/login', {
            message: message
        })
    }
});

app.post("/api/register", urlEncoded, async (req, res) => {
    try {
        let user = await User.create({
            username: req.body.username,
            password: req.body.password,
        });
        res.redirect('/login');
    } catch (error) {
        res.render('users/register', {
            message: {
                type: 'error',
                message: 'An error occurred while registering the user.'
            }
        })
    }
});

app.get("/api/logout", urlEncoded, async (req, res) => {
    if (req.session.userId) {
        req.session.destroy();
        res.redirect('/');
    }
})

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// API Biodata
app.post("/api/biodata", urlEncoded, async (req, res) => {
    try {
        let biodata = await Biodata.create({
            userId: parseInt(req.session.userId),
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            address: req.body.address,
            phoneNumber: req.body.phoneNumber
        })
        res.redirect('/');
    } catch (error) {
        if (error.name === 'SequelizeUniqueConstraintError') {
            res.render('biodata/create', {
                session: req.session,
                errorModal: true,
                message: {
                    type: 'error',
                    message: `You can't create new biodata`
                }
            });
        } else {
            res.status(500).json({ error: 'Internal server error' });
        }
    }
})

app.post("/api/biodata/edit/:id", urlEncoded, async (req, res) => {
    try {
        const user = await User.findByPk(req.session.userId);
        if (!user) {
            return res.redirect('/')
        }
        const biodata = await Biodata.findOne({
            where: {
                userId: user.id
            }
        })
        biodata.firstName = req.body.firstName;
        biodata.lastName = req.body.lastName;
        biodata.email = req.body.email;
        biodata.address = req.body.address;
        biodata.phoneNumber = req.body.phoneNumber;

        await biodata.save();

        res.redirect('/')
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error")
    }
})

app.get("/api/biodata/delete/:id", async (req, res) => {
    try {
        const user = await User.findByPk(req.session.userId);
        if (!user) {
            return res.redirect('/')
        }

        const biodata = await Biodata.findOne({
            where: {
                userId: user.id
            }
        })

        if (!biodata) {
            return res.redirect('/');
        }

        await biodata.destroy();
        res.redirect('/')

    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error")
    }
})
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// API History

app.post("/api/history", urlEncoded, async (req, res) => {
    console.log(req.session.userId);
    console.log(req.body.history);
    console.log(req.body.timePlayed);
    console.log(req.body.lastPlayed);

    try {
        res.locals.moment = moment;
        let history = await History.create({
            userId: parseInt(req.session.userId),
            history: req.body.history,
            timePlayed: req.body.timePlayed,
            lastPlayed: req.body.lastPlayed
        })
        res.redirect('/');
    } catch (error) {
        res.render('history/create', {
            session: req.session,
            message: {
                type: 'error',
                message: 'An error occurred while creating the history record.'
            }
        })
    }
})


app.post("/api/history/edit/:id", urlEncoded, async (req, res) => {
    try {
        // Found matched history
        let history = await History.findByPk(req.body.id);

        // Update the history with the new values from the request body
        history.history = req.body.history,
            history.timePlayed = req.body.timePlayed,
            history.lastPlayed = req.body.lastPlayer

        // Save the updated biodata record
        await history.save();

        res.redirect('/')
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error")
    }
})


app.get("/api/history/delete/:id", async (req, res) => {
    try {
        const deletedRows = await History.destroy({
            where: { id: req.params.id }
        });
        if (deletedRows > 0) {
            return res.redirect("/");
        } else {
            return res.redirect("/");
        }
    } catch (error) {
        console.error(error);
        return res.redirect("/");
    }
});
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

app.listen(port, () => {
    console.table({
        url: `http://localhost:${port}`
    });
});